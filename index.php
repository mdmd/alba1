﻿<!DOCTYPE html>
<html>
<title>کلاس  php</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Open Sans", sans-serif}
</style>
<body class="w3-theme-l5" dir = "rtl" >

<!-- Navbar -->
<?php

include "navbar.php";

?>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">

</div>

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:1400px;margin-top:90px">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->

<?php
include "leftColumn.php" ;
?>
      
    <!-- End Left Column -->
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    

      <div class="w3-row-padding w3-card w3-white w3-round w3-margin"><br>
        <h4>معرفی PHP</h4><br>
        <hr class="w3-clear">
        <p>PHP یک زبان برنامه نویسی تحت وب می باشد.راسموس لردورف برای ایجاد صفحه شخصی خود این زبان را ابداع کرد و نام آن را Personal Home Page قرار داد.بعد ها اندی گاتسمن و زیو سوراسکی این زبان را توسعه دادند و نام آن را Hypertext Preprocessor به معنای پیش پردازنده ابر متن قرار دادند و شرکت زند تکنولوژی را در کشور اسرائیل تاسیس کردند.</P>
          <div class="w3-row-padding" style="margin:0 -16px">

        </div> 
      </div>
      
	  
	  
	        <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
         <h4>معرفی نرم افزارها</h4><br>
        <hr class="w3-clear">
        <p>جهت برنامه نویسی PHP پیشنهاد می شود از نرم افزار PhpStorm ساخته شده توسط شرکت JetBrains و نرم افزار XAMP استفاده نمایید.</p>
        <div class="w3-row-padding" style="margin:0 -16px">
            <div class="w3-half">
              <img src="w3images/xamp.jpg" style="width:100%" alt="Northern Lights" class="w3-margin-bottom">
            </div>
            <div class="w3-half">
              <img src="w3images/phpstorm.png" style="width:100%" alt="Nature" class="w3-margin-bottom">
          </div>
        </div>
      </div>
	  
	  
	  

      <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
        <h4>معرفی GitLab</h4><br>
        <hr class="w3-clear">
        <p>پیشنهاد می شود از این پس  کدهای خود را در محیط gitlab ذخیره نمایید این محیط به شما امکان دسترسی راحت و بازیابی کدها و... را می دهد.</p>
        <img src="w3images/gitlab.jpg" style="width:100%" class="w3-margin-bottom">

      </div> 
      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
<?php

include "rightColumn.php";

?>
      

    

    <!-- End Right Column -->

    
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
<br>

<!-- Footer -->
<?php

include "footer.php" ;

?>
 
<script>
// Accordion
function myFunction(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-theme-d1";
    } else { 
        x.className = x.className.replace("w3-show", "");
        x.previousElementSibling.className = 
        x.previousElementSibling.className.replace(" w3-theme-d1", "");
    }
}

// Used to toggle the menu on smaller screens when clicking on the menu button
function openNav() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>

</body>
</html> 
